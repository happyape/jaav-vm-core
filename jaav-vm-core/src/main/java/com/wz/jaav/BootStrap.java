package com.wz.jaav;

import com.beust.jcommander.JCommander;
import com.wz.jaav.classreader.classFile.MethodInfo;
import com.wz.jaav.classreader.classFile.MethodInfoPool;
import com.wz.jaav.classreader.classFile.file.ClassFile;
import com.wz.jaav.command.Command;
import com.wz.jaav.instruction.Interpreter;
import com.wz.jaav.search.NewDirEntry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BootStrap {

    public static ClassFile cf = new ClassFile();

    public static void main(String[] arg) throws IOException {
        String[] args = { "-cp", "C:\\Users\\Administrator\\Desktop", "com.wz.jaav.HelloWorld"};
        Command command = new Command();
        JCommander.newBuilder()
                .addObject(command)
                .build()
                .parse(args);
        if (command.isHelpFlag()){
            System.out.println("jaav [-options] class [args...]");
            return;
        }
        if (command.isVersionFlag()){
            System.out.println("version 1.0");
            return;
        }
        if (command.getArgs()!=null && command.getArgs().size()!=0){
            if (command.getArgs().get(0).equals("-cp")){
                command.setCpOption(command.getArgs().get(1));
                command.setClazz(command.getArgs().get(2));
                List<String> theArgs = new ArrayList<String>();
                for (int i = 3; i < command.getArgs().size() ; i++) {
                    theArgs.add(command.getArgs().get(i));
                }
                command.setArgs(theArgs);
            }
        }

        NewDirEntry newDirEntry = new NewDirEntry(command.getCpOption());
        byte[] classByte = newDirEntry.readClassData(command.getClazz());

        cf.build(classByte);
        System.out.println(cf.getMagic().toString());
        System.out.println(cf.getMinorVersion().toString());
        System.out.println(cf.getMajorVersion().toString());
        cf.getConstantPool().printInfos();
        System.out.println(cf.getAccessFlags());
        System.out.println(cf.getThisClass());
        System.out.println(cf.getSuperClass());
        System.out.println(cf.getInterfaces());
        cf.getFieldInfoPool().printInfos();
        cf.getMethodInfoPool().printInfos();

//        Interpreter interpreter = new Interpreter();
//        MethodInfoPool pool = cf.getMethodInfoPool();
//
//        interpreter.interpret(pool.getMethodInfoById((short) 11));
//        System.out.println(Arrays.toString(classByte));
//        System.out.println(CommonUtils.bytesToHex(classByte));
    }


}