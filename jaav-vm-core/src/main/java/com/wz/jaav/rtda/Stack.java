package com.wz.jaav.rtda;

import java.util.EmptyStackException;

public class Stack {
    private int maxSize;
    private int size;
    private Frame top;

    public Stack(int maxSize) {
        this.maxSize = maxSize;
    }

    public void pushFrame(Frame frame){
        if (this.size >= this.maxSize){
            throw new StackOverflowError("JAAV STACK OVER FLOW!");
        }
        if (this.top != null){
            frame.setLower(this.top);
        }
        this.top = frame;
        this.size = this.size + 1;
    }

    public Frame popFrame(){
        if (this.top == null){
            throw new EmptyStackException();
        }
        Frame popFrame = top;
        this.top = popFrame.getLower();
        popFrame.setLower(null);
        this.size = this.size - 1;
        return popFrame;
    }

    public Frame getTop() {
        return top;
    }

    public static void main(String[] args) {
//        Stack stack = new Stack(10);
//        Frame frame = new Frame(10, 10);
//        frame.getLocalVars().setInt(0, 10);
//        frame.getOperandStack().pushInt(100);
//        stack.pushFrame(frame);
//
//        Frame popFrame = stack.popFrame();
//        System.out.println(popFrame.getLocalVars().getInt(0));
//        System.out.println(popFrame.getLower());
//        System.out.println(popFrame.getOperandStack().popInt());
    }
}