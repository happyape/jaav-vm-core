package com.wz.jaav.rtda.heap;

public enum  AccessFlagEnum {
    ACC_PUBLIC(0X0001),        //CLASS,FIELD,METHOD
    ACC_PRIVATE(0X0002),       //      FIELD,METHOD
    ACC_PROTECTED(0X0004),     //      FIELD,METHOD
    ACC_STATIC(0X0008),        //      FIELD,METHOD
    ACC_FINAL(0X0010),         //CLASS,FIELD,METHOD
    ACC_SUPER(0X0020),         //CLASS
    ACC_SYNCHRONIZED(0X0020),  //            METHOD
    ACC_VOLATILE(0X0040),      //      FIELD,METHOD
    ACC_BRIDGE(0X0040),        //      FIELD
    ACC_TRANSIENT(0X0080),     //            METHOD
    ACC_VARARGS(0X0080),       //      FIELD
    ACC_NATIVE(0X0100),        //            METHOD
    ACC_INTERFACE(0X0200),     //CLASS
    ACC_ABSTRACT(0X0400),      //CLASS
    ACC_STRICT(0X0800),        //            METHOD
    ACC_SYNTHETIC(0X1000),     //CLASS,FIELD,METHOD
    ACC_ANNOTATION(0X2000),    //CLASS
    ACC_ENUM(0X4000);          //CLASS,FIELD

    private int sign;

    AccessFlagEnum(int sign) {
        this.sign = sign;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }
}