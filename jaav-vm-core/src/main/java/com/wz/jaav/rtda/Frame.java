package com.wz.jaav.rtda;

public class Frame {
    private Frame lower;
    private LocalVars localVars;
    private OperandStack operandStack;
    private Thread thread;
    private int nextPc;

    public Frame(Thread thread, int maxLocalVars, int maxOperandStack) {
        this.thread = thread;
        this.localVars = new LocalVars(maxLocalVars);
        this.operandStack = new OperandStack(maxOperandStack);
    }

    public void setLower(Frame lower) {
        this.lower = lower;
    }

    public Frame getLower() {
        return lower;
    }

    public LocalVars getLocalVars() {
        return localVars;
    }

    public OperandStack getOperandStack() {
        return operandStack;
    }

    public int getNextPc() {
        return nextPc;
    }

    public void setNextPc(int nextPc) {
        this.nextPc = nextPc;
    }

    public Thread getThread() {
        return thread;
    }

    @Override
    public String toString() {
        return "Frame{" +
                "localVars=" + localVars +
                ", operandStack=" + operandStack +
                ", nextPc=" + nextPc +
                '}';
    }
}