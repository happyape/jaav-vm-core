package com.wz.jaav.rtda;

import com.wz.jaav.rtda.heap.Object;

import java.util.Arrays;

public class Slot {
    private byte[] num = new byte[4];
    private Object ref = new Object();

    public Slot() {
    }

    public void setNum(byte[] num) {
        this.num = num;
    }

    public byte[] getNum() {
        return num;
    }

    public Object getRef() {
        return ref;
    }

    public void setRef(Object ref) {
        this.ref = ref;
    }

    @Override
    public String toString() {
        return "Slot{" +
                "num=" + Arrays.toString(num) +
                ", ref=" + ref +
                '}';
    }
}