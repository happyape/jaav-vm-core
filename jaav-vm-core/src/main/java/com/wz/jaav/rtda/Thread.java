package com.wz.jaav.rtda;

public class Thread {
    private int pc;
    private Stack stack;

    public Thread() {
        this.stack = new Stack(1024);
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public int getPc() {
        return this.pc;
    }

    public void pushFrame(Frame frame) {
        this.stack.pushFrame(frame);
    }

    public Frame popFrame() {
        return this.stack.popFrame();
    }

    public Frame currentFrame() {
        return this.stack.getTop();
    }

    public Frame getNewFrame(int maxLocalVars, int maxOperandStack){
        return new Frame(this, maxLocalVars, maxOperandStack);
    }
}