package com.wz.jaav.search;

import com.wz.jaav.util.CommonUtils;

import java.io.File;
import java.io.IOException;

public class NewDirEntry implements Entry{

    private String absDir;

    public NewDirEntry(String absDir) {
        this.absDir = absDir;
    }

    public byte[] readClassData(String className) {
        try {
            return CommonUtils.toByteArray(CommonUtils.getClassAbsPath(absDir, className));
        } catch (IOException e) {
            System.out.println("read class file io error!");
        }
        return new byte[0];
    }

    public String String() {
        return absDir;
    }

    private static boolean isAbs(String filePath){
        File file = new File(filePath);
        return file.isDirectory() && file.isAbsolute();
    }
}