package com.wz.jaav.search;

public interface Entry {
    byte[] readClassData(String className);
    String String();
}