package com.wz.jaav.classreader;

import java.io.ByteArrayInputStream;

public class ClassReader {

    private byte[] classData;
    private ByteArrayInputStream is;
    private int index = 0;

    public ClassReader(byte[] classData){
        this.classData = classData;
        is = new ByteArrayInputStream(classData);
    }

    public byte[] doRead(int length){
        byte[] data = new byte[length];
        is.reset();
        is.read(data,0, length);
        is.mark(index + length);
        return data;
    }
}