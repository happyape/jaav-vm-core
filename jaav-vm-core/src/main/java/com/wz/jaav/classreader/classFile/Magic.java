package com.wz.jaav.classreader.classFile;

import com.wz.jaav.util.CommonUtils;

public class Magic {
    private int len = 4;
    private byte[] magic; //32

    public Magic() {}

    public int getLen() {
        return len;
    }

    public byte[] getMagic() {
        return magic;
    }

    public void setMagic(byte[] magic) {
        this.magic = magic;
    }

    @Override
    public String toString() {
        return CommonUtils.bytesToHex(this.magic);
    }
}