package com.wz.jaav.classreader.classFile.attribute;

import com.wz.jaav.util.CommonUtils;

import java.io.UTFDataFormatException;

public class Code {
    private byte[] codeStr;

    public Code(byte[] codeStr) {
        this.codeStr = codeStr;
    }

    public byte[] getCodeStr() {
        return codeStr;
    }

    public String getString() {
        return CommonUtils.bytesToHex(codeStr);
    }

    @Override
    public String toString() {
        return "Code{" +
                "codeStr=" + getString() +
                '}';
    }
}