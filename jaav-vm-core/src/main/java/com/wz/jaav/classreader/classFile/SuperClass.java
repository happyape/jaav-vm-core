package com.wz.jaav.classreader.classFile;

import com.wz.jaav.util.CommonUtils;

public class SuperClass {
    private int len = 2;
    private byte[] superClass; //16

    public int getLen() {
        return len;
    }

    public byte[] getSuperClass() {
        return superClass;
    }

    public void setSuperClass(byte[] superClass) {
        this.superClass = superClass;
    }

    @Override
    public String toString() {
        return "super class:" + CommonUtils.bytesToHex(this.superClass);
    }
}