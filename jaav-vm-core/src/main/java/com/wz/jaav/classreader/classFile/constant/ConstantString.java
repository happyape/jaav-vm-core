package com.wz.jaav.classreader.classFile.constant;

public class ConstantString extends ConstantInfo{
    public static final byte TAG_STRING_INFO = 8;

    private short stringIndex;

    public ConstantString(short stringIndex) {
        this.stringIndex = stringIndex;
    }

    @Override
    public String toString() {
        return "ConstantString{type=" + TAG_STRING_INFO +
                ", stringIndex=" + stringIndex +
                '}';
    }
}