package com.wz.jaav.classreader.classFile;

import com.wz.jaav.util.CommonUtils;

public class MinorVersion {
    private int len = 2;
    private byte[] minorVersion; //16

    public MinorVersion() {
    }

    public int getLen() {
        return len;
    }

    public void setMinorVersion(byte[] minorVersion) {
        this.minorVersion = minorVersion;
    }

    public byte[] getMinorVersion() {
        return minorVersion;
    }

    @Override
    public String toString() {
        return CommonUtils.bytesToHex(this.minorVersion);
    }
}