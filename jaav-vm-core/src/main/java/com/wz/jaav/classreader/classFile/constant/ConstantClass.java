package com.wz.jaav.classreader.classFile.constant;

public class ConstantClass extends ConstantInfo {
    public static final byte TAG_CLASS_INFO = 7;

    private short clazzStringIndex;

    public ConstantClass(short clazzStringIndex) {
        this.clazzStringIndex = clazzStringIndex;
    }

    @Override
    public String toString() {
        return "ConstantClass{type=" + TAG_CLASS_INFO +
                ", clazzStringIndex=" + clazzStringIndex +
                '}';
    }
}