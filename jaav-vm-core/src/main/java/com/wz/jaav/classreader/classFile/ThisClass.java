package com.wz.jaav.classreader.classFile;

import com.wz.jaav.util.CommonUtils;

public class ThisClass {
    private int len = 2;
    private byte[] thisClass; //16

    public int getLen() {
        return len;
    }

    public byte[] getThisClass() {
        return thisClass;
    }

    public void setThisClass(byte[] thisClass) {
        this.thisClass = thisClass;
    }

    @Override
    public String toString() {
        return "this class:" + CommonUtils.bytesToHex(this.thisClass);
    }
}