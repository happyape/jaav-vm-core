package com.wz.jaav.classreader.classFile;

import com.wz.jaav.util.CommonUtils;

public class AccessFlags {
    private int len = 2;
    private byte[] flag; //16

    public int getLen() {
        return len;
    }

    public byte[] getFlag() {
        return flag;
    }

    public void setFlag(byte[] flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "access flag:" + CommonUtils.bytesToHex(this.flag);
    }
}