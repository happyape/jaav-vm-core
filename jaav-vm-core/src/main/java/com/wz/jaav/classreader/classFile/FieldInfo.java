package com.wz.jaav.classreader.classFile;

import com.wz.jaav.classreader.ClassReader;
import com.wz.jaav.classreader.classFile.attribute.Attribute;
import com.wz.jaav.util.CommonUtils;

public class FieldInfo {
    //单纯计数用，非文件属性
    private short count;

    private short accessFlag;
    private short nameIndex;
    private short descriptorIndex;
    private short attributeCount;
    private Attribute[] infos;

    public void build(ClassReader cr) {
        byte[] af = cr.doRead(2);
        System.out.println("read:" + CommonUtils.bytesToHex(af));
        accessFlag = CommonUtils.byteToShort(af);
        byte[] ni = cr.doRead(2);
        System.out.println("read:" + CommonUtils.bytesToHex(ni));
        nameIndex = CommonUtils.byteToShort(ni);
        byte[] di = cr.doRead(2);
        System.out.println("read:" + CommonUtils.bytesToHex(di));
        descriptorIndex = CommonUtils.byteToShort(di);
        byte[] ac = cr.doRead(2);
        System.out.println("read:" + CommonUtils.bytesToHex(ac));
        attributeCount = CommonUtils.byteToShort(ac);
        infos = new Attribute[attributeCount];
        for (int i = 0; i < attributeCount; i++) {
            buildAttrs(cr);
            count++;
        }
    }

    private void buildAttrs(ClassReader cr) {
        //FIXME 暂未实现
    }

    @Override
    public String toString() {
        return "FieldInfo{" +
                "accessFlag=" + accessFlag +
                ", nameIndex=" + nameIndex +
                ", descriptorIndex=" + descriptorIndex +
                ", attributeCount=" + attributeCount +
                '}';
    }
}