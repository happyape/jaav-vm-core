package com.wz.jaav.classreader.classFile.constant;

public class ConstantNameAndType extends ConstantInfo{
    public static final byte TAG_NAME_AND_TYPE = 12;

    private short nameIndex; //字段或方法常量项索引
    private short nameDesIndex;//字段或方法描述索引

    public ConstantNameAndType(short nameIndex, short nameDesIndex) {
        this.nameIndex = nameIndex;
        this.nameDesIndex = nameDesIndex;
    }

    public short getNameIndex() {
        return nameIndex;
    }

    public short getNameDesIndex() {
        return nameDesIndex;
    }

    @Override
    public String toString() {
        return "ConstantNameAndType{type=" + TAG_NAME_AND_TYPE +
                ", nameIndex=" + nameIndex +
                ", nameDesIndex=" + nameDesIndex +
                '}';
    }
}