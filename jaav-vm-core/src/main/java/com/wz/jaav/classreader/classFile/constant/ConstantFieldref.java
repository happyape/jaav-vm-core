package com.wz.jaav.classreader.classFile.constant;

public class ConstantFieldref extends ConstantInfo{
    public static final byte TAG_FEILD_REF = 9;

    private short classInfoIndex;
    private short nameAndTypeIndex;

    public ConstantFieldref(short classInfoIndex, short nameAndTypeIndex) {
        this.classInfoIndex = classInfoIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }

    public short getClassInfoIndex() {
        return classInfoIndex;
    }

    public short getNameAndTypeIndex() {
        return nameAndTypeIndex;
    }

    @Override
    public String toString() {
        return "ConstantFieldref{type=" + TAG_FEILD_REF +
                ", classInfoIndex=" + classInfoIndex +
                ", nameAndTypeIndex=" + nameAndTypeIndex +
                '}';
    }
}