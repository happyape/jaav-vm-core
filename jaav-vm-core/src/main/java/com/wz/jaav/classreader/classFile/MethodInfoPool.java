package com.wz.jaav.classreader.classFile;

import com.wz.jaav.classreader.ClassReader;
import com.wz.jaav.util.CommonUtils;

public class MethodInfoPool {
    //单纯计数用，非文件属性
    private short count;

    private short methodInfoCount;
    private MethodInfo[] infos;

    public void build(ClassReader cr) {
        byte[] af = cr.doRead(2);
        System.out.println("method read:" + CommonUtils.bytesToHex(af));
        methodInfoCount = CommonUtils.byteToShort(af);
        infos = new MethodInfo[methodInfoCount];
        for (int i = 0; i < methodInfoCount; i++) {
            MethodInfo methodInfo = new MethodInfo();
            methodInfo.build(cr);
            infos[i] = methodInfo;
            count++;
        }
    }

    public MethodInfo[] getInfos() {
        return infos;
    }

    public MethodInfo getMethodInfoById(short nameIndex){
        for (MethodInfo info : infos) {
            if (info.getNameIndex() == nameIndex){
                return info;
            }
        }
        System.out.println("cant find the method [method name index -->" + nameIndex + "]");
        return null;
    }

    public String printInfos() {
        System.out.println("Method total:" + count);
        int constantInfoCount = 1;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < infos.length; i++) {
            if (infos[i] != null) {
                System.out.println("    Method id:" + (i + 1));
                infos[i].printInfos();
            }
        }
        return sb.toString();
    }
}