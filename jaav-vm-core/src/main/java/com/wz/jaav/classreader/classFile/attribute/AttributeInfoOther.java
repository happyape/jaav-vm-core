package com.wz.jaav.classreader.classFile.attribute;

public class AttributeInfoOther extends AttributeInfo {

    private String type;

    public AttributeInfoOther(String type) {
        this.type = type;
    }

    @Override
    public void printInfos(String placeholder) {
        System.out.println(placeholder + "AttributeInfoOther:" + this.type);
    }
}
