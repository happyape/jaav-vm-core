package com.wz.jaav.classreader.classFile;

import com.wz.jaav.classreader.ClassReader;
import com.wz.jaav.classreader.classFile.constant.ConstantInfo;
import com.wz.jaav.util.CommonUtils;

public class Interfaces {
    private short totalInterfaces;

    public void build(ClassReader cr){
        totalInterfaces = CommonUtils.byteToShort(cr.doRead(2));
        if (totalInterfaces!=0){
            System.out.println("Need Fix!");
        }
    }

    @Override
    public String toString() {
        return "interfaces: " + totalInterfaces + "";
    }
}