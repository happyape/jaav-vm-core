package com.wz.jaav.classreader.classFile;

import com.wz.jaav.classreader.ClassReader;
import com.wz.jaav.util.CommonUtils;

public class FieldInfoPool {
    //单纯计数用，非文件属性
    private short count;

    private short fieldInfoCount;
    private FieldInfo[] infos;

    public void build(ClassReader cr) {
        byte[] af = cr.doRead(2);
        System.out.println("read:" + CommonUtils.bytesToHex(af));
        fieldInfoCount = CommonUtils.byteToShort(af);
        infos = new FieldInfo[fieldInfoCount];
        for (int i = 0; i < fieldInfoCount; i++) {
            FieldInfo mermberInfo = new FieldInfo();
            mermberInfo.build(cr);
            infos[i] = mermberInfo;
            count++;
        }
    }

    public FieldInfo[] getInfos() {
        return infos;
    }

    public String printInfos() {
        System.out.println("Field total:" + count);
        int constantInfoCount = 1;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < infos.length; i++) {
            if (infos[i] != null) {
                sb.append("id:").append(i).append("--").append(infos[i].toString());
                System.out.println("    id:" + i + "--" + infos[i].toString());
            }
        }
        return sb.toString();
    }
}