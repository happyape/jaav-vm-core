package com.wz.jaav.classreader.classFile.file;

import com.wz.jaav.classreader.ClassReader;
import com.wz.jaav.classreader.classFile.*;
import com.wz.jaav.classreader.classFile.attribute.Attribute;

import java.io.IOException;

public class ClassFile {
    private Magic magic;
    private MinorVersion minorVersion;
    private MajorVersion majorVersion;
    private ConstantPool constantPool;
    private AccessFlags accessFlags;
    private ThisClass thisClass;
    private SuperClass superClass;
    private Interfaces interfaces;
    private FieldInfoPool fieldInfoPool;
    private MethodInfoPool methodInfoPool;

    public ClassFile() {
    }

    public void build(byte[] classData) throws IOException {
        ClassReader cr = new ClassReader(classData);
        magic = new Magic();
        byte[] magicData = cr.doRead(magic.getLen());
        magic.setMagic(magicData);

        minorVersion = new MinorVersion();
        byte[] minorVersionData = cr.doRead(minorVersion.getLen());
        minorVersion.setMinorVersion(minorVersionData);

        majorVersion = new MajorVersion();
        byte[] majorVersionData = cr.doRead(majorVersion.getLen());
        majorVersion.setMajorVersion(majorVersionData);

        constantPool = new ConstantPool();
        constantPool.build(cr);

        accessFlags = new AccessFlags();
        byte[] af = cr.doRead(accessFlags.getLen());
        accessFlags.setFlag(af);

        thisClass = new ThisClass();
        byte[] tc = cr.doRead(thisClass.getLen());
        thisClass.setThisClass(tc);

        superClass = new SuperClass();
        byte[] sc = cr.doRead(superClass.getLen());
        superClass.setSuperClass(sc);

        interfaces = new Interfaces();
        interfaces.build(cr);

        fieldInfoPool = new FieldInfoPool();
        fieldInfoPool.build(cr);

        methodInfoPool = new MethodInfoPool();
        methodInfoPool.build(cr);
    }


    public Magic getMagic() {
        return magic;
    }

    public MinorVersion getMinorVersion() {
        return minorVersion;
    }

    public MajorVersion getMajorVersion() {
        return majorVersion;
    }

    public ConstantPool getConstantPool() {
        return constantPool;
    }

    public AccessFlags getAccessFlags() {
        return accessFlags;
    }

    public ThisClass getThisClass() {
        return thisClass;
    }

    public SuperClass getSuperClass() {
        return superClass;
    }

    public com.wz.jaav.classreader.classFile.Interfaces getInterfaces() {
        return interfaces;
    }

    public FieldInfoPool getFieldInfoPool() {
        return fieldInfoPool;
    }

    public MethodInfoPool getMethodInfoPool() {
        return methodInfoPool;
    }
}