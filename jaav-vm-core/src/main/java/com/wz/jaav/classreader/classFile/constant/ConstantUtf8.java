package com.wz.jaav.classreader.classFile.constant;

import com.wz.jaav.util.CommonUtils;

import java.io.UTFDataFormatException;
import java.util.Arrays;

public class ConstantUtf8 extends ConstantInfo{
    public static final byte TAG_UTF_8 = 1;

    private short length;
    private byte[] str;

    public ConstantUtf8(short length, byte[] str) {
        this.length = length;
        this.str = str;
    }

    public short getLength() {
        return length;
    }

    public byte[] getStr() {
        return str;
    }

    public String getString(){
        try {
            return CommonUtils.readUTF(length, str);
        } catch (UTFDataFormatException e) {
            return "read mutf-8 exception!";
        }
    }

    @Override
    public String toString() {
        return "ConstantUtf8{type=" + TAG_UTF_8 +
                ", length=" + length +
                ", str=" + getString() +
                '}';
    }
}