package com.wz.jaav.classreader.classFile.constant;

public class ConstantMethodref extends ConstantInfo{
    public static final byte TAG_METHOD_REF = 10;
    private short classInfoIndex;
    private short nameAndTypeIndex;

    public ConstantMethodref(short classInfoIndex, short nameAndTypeIndex) {
        this.classInfoIndex = classInfoIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }

    public short getClassInfoIndex() {
        return classInfoIndex;
    }

    public short getNameAndTypeIndex() {
        return nameAndTypeIndex;
    }

    @Override
    public String toString() {
        return "ConstantMethodref{type=" + TAG_METHOD_REF +
                ",classInfoIndex=" + classInfoIndex +
                ", nameAndTypeIndex=" + nameAndTypeIndex +
                '}';
    }
}