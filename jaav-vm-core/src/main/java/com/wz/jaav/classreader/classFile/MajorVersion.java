package com.wz.jaav.classreader.classFile;

import com.wz.jaav.util.CommonUtils;

public class MajorVersion {
    private int len = 2;
    private byte[] majorVersion; //16

    public MajorVersion() {
    }

    public int getLen() {
        return len;
    }

    public byte[] getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(byte[] majorVersion) {
        this.majorVersion = majorVersion;
    }

    @Override
    public String toString() {
        return CommonUtils.bytesToHex(this.majorVersion);
    }
}