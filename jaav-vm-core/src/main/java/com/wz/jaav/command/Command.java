package com.wz.jaav.command;

import com.beust.jcommander.Parameter;

import java.util.List;

public class Command {
    @Parameter(names = { "-help", "-?" }, help = true)
    private boolean helpFlag;
    @Parameter(names = { "-version", "-v" }, help = true, description = "print version and exit")
    private boolean versionFlag;
    private String cpOption;
    private String clazz;
    @Parameter
    private List<String> args;

    public boolean isHelpFlag() {
        return helpFlag;
    }

    public void setHelpFlag(boolean helpFlag) {
        this.helpFlag = helpFlag;
    }

    public boolean isVersionFlag() {
        return versionFlag;
    }

    public void setVersionFlag(boolean versionFlag) {
        this.versionFlag = versionFlag;
    }

    public String getCpOption() {
        return cpOption;
    }

    public void setCpOption(String cpOption) {
        this.cpOption = cpOption;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<String> args) {
        this.args = args;
    }
}