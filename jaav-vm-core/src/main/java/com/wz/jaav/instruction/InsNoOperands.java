package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public abstract class InsNoOperands implements Instuction{
    @Override
    public void fetchOperands(ByteCodeReader reader) {

    }

    @Override
    public void execute(Frame frame) {

    }

    void _istore(Frame frame, int index){
        int val = frame.getOperandStack().popInt();
        System.out.println("--->oper stack:pop int:" + val);
        frame.getLocalVars().setInt(index, val);
        System.out.println("--->local vars:set int:" + val);
    }

    void _iload(Frame frame, int index){
        int val = frame.getLocalVars().getInt(index);
        System.out.println("--->local vars:get int:" + val);
        frame.getOperandStack().pushInt(val);
        System.out.println("--->oper stack:push int:" + val);
    }
}