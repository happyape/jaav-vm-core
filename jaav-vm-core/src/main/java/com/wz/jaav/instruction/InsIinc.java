package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;
import com.wz.jaav.rtda.LocalVars;

/**
 * i_const_0
 */
public class InsIinc implements Instuction {
    private int index;
    private int constValue;

    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.index = reader.readByte();
        this.constValue = reader.readByte();
    }

    @Override
    public void execute(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        int val = localVars.getInt(this.index);
        val = val + this.constValue;
        localVars.setInt(this.index, val);

        System.out.println("--->Iinc: " + val + " + " + this.constValue);
    }
}