package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

/**
 * i_const_0
 */
public class InsIcounst0 extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushInt(0);
        System.out.println("--->icounst_0");
    }
}