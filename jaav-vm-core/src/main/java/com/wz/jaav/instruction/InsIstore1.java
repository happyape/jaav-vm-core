package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsIstore1 extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        System.out.println("--->istore_1");
        super._istore(frame, 1);
    }
}