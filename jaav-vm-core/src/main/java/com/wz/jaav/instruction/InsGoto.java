package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsGoto extends InsBranch {

    @Override
    public void execute(Frame frame) {
        super.branch(frame, super.getOffSet());
    }
}