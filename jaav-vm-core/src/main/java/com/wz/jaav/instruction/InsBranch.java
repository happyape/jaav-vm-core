package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;
import com.wz.jaav.util.ByteArrayConveter;

public abstract class InsBranch implements Instuction{

    private int offSet;

    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.offSet = reader.readOffset();
        System.out.println("");
    }

    @Override
    public void execute(Frame frame) {

    }

    public int getOffSet() {
        return offSet;
    }

    public void branch(Frame frame, int offSet){
        int pc = frame.getThread().getPc();
        int nextPc = pc + offSet;
        frame.setNextPc(nextPc);
        if (nextPc == 20){
            System.out.println();
        }
        System.out.println("--->jump to: " + nextPc);
    }

    public static void main(String[] args) {
        byte[] a = new byte[]{0,0,19,-70};
        System.out.println(ByteArrayConveter.getInt(a,0));
    }
}