package com.wz.jaav.instruction;

import com.wz.jaav.util.ByteArrayConveter;

public class ByteCodeReader {
    private byte[] code;
    private int pc;

    public void reSet(byte[] code, int pc){
        this.code = code;
        this.pc = pc;
    }

    public byte readByte(){
        byte b = this.code[pc];
        this.pc = this.pc + 1;
        return b;
    }

    public int getPc() {
        return pc;
    }

    public short readShort(){
        byte high = readByte();
        byte lower = readByte();
        short readShort = ByteArrayConveter.getShort(new byte[]{high, lower}, 0);
        return readShort;
    }

    public short readOffset(){
        byte high = readByte();
        byte lower = readByte();
        return (short)((high << 8) | lower);
    }

    public int readInt(){
        byte one = readByte();
        byte two = readByte();
        byte three = readByte();
        byte four = readByte();
        int readInt = ByteArrayConveter.getInt(new byte[]{one, two, three, four}, 0);
        return readInt;
    }

    public static void main(String[] args) {
//        ByteCodeReader reader = new ByteCodeReader();
//        byte[] code = new byte[]{0,0,0,-1};
//        reader.reSet(code, 0);
//        System.out.println(reader.readByte() + ":" + reader.getPc());
//        System.out.println(reader.readShort() + ":" + reader.getPc());
//        System.out.println(reader.readInt() + ":" + reader.getPc());
        byte a = -1;
        System.out.println(a & 0xFF);
    }
}