package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public interface Instuction {

    void fetchOperands(ByteCodeReader reader);
    void execute(Frame frame);
}