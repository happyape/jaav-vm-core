package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsBipush implements Instuction{
    private int val;

    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.val = reader.readByte();
    }

    @Override
    public void execute(Frame frame) {
        System.out.println("--->bipush:" + this.val);
        frame.getOperandStack().pushInt(this.val);
    }
}