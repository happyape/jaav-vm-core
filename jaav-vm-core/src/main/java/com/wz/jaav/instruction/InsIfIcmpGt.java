package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;
import com.wz.jaav.rtda.OperandStack;

public class InsIfIcmpGt extends InsBranch{

    @Override
    public void execute(Frame frame) {
        OperandStack stack = frame.getOperandStack();
        int val_2 = stack.popInt();
        int val_1 = stack.popInt();
        if (val_1 > val_2){
            super.branch(frame, super.getOffSet());
        }
        System.out.println("--->IfIcmpGt:" + val_1 + " " + val_2);
    }
}