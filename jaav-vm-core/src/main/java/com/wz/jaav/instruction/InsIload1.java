package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsIload1 extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        System.out.println("--->iload_1");
        super._iload(frame, 1);
    }
}