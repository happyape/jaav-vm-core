package com.wz.jaav.instruction;

import com.sun.org.apache.regexp.internal.RE;

public class Instuctions {
    public static Instuction getNewInstuction(int operCode){
        switch (operCode){
            case 0x03:
                return new InsIcounst0();
            case 0x3C:
                return new InsIstore1();
            case 0x04:
                return new InsIcounst1();
            case 0x3D:
                return new InsIstore2();
            case 0x1C:
                return new InsIload2();
            case 0x10:
                return new InsBipush();
            case 0xA3:
                return new InsIfIcmpGt();
            case 0x1B:
                return new InsIload1();
            case 0x60:
                return new InsIadd();
            case 0x84:
                return new InsIinc();
            case 0xA7:
                return new InsGoto();
            case 0xB1:
                return new InsReturn();
                    default: return null;
        }
    }
}