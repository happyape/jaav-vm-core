package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsIload2 extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        System.out.println("--->iload_2");
        super._iload(frame, 2);
    }
}