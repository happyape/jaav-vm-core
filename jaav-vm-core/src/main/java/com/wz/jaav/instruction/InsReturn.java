package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsReturn extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        frame.getThread().popFrame();
        System.out.println("--->return");
    }
}