package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public class InsIstore2 extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        System.out.println("--->istore_2");
        super._istore(frame, 2);
    }
}