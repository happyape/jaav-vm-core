package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

/**
 * i_const_1
 */
public class InsIcounst1 extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushInt(1);
        System.out.println("--->icounst_1");
    }
}