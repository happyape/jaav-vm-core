package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;

public abstract class InsIndex8 implements Instuction{

    private int index;

    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.index = reader.readByte();
    }

    @Override
    public void execute(Frame frame) {

    }
}