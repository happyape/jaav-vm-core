package com.wz.jaav.instruction;

import com.wz.jaav.rtda.Frame;
import com.wz.jaav.rtda.OperandStack;

public class InsIadd extends InsNoOperands{

    @Override
    public void execute(Frame frame) {
        OperandStack stack = frame.getOperandStack();
        int val_2 = stack.popInt();
        int val_1 = stack.popInt();

        stack.pushInt(val_1 + val_2);

        System.out.println("--->iadd: " + val_1 + " " + val_2);
    }
}