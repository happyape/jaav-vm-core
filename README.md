# jaav

#### 介绍
一个java版本的虚拟机,用于解释执行字节码文件,
其中包含一个字节码reader，常量池，一些指令集以及一个解释器，一个thread的模型

当然，类和对象，方法调用和返回，暂时都是没实现的，哈哈哈！有时间再补咯！

#### 功能

目前对付这个类是一点没有问题的:


```
package com.wz.jaav;

public class HelloWorld {

    public static void main(String[] args) {
        int sum = 0;
        for (int i=1; i <=100; i++){
            sum += i;
        }
    }
}
```

#### 参考资料

1.  关于如何解析字节码文件，感谢来自csdn的这篇[博客](https://blog.csdn.net/qq_37206105/article/details/91354385?ops_request_misc=&request_id=&biz_id=102&utm_term=%E6%9F%A5%E7%9C%8Bjava%E5%AD%97%E8%8A%82%E7%A0%81%20%20%E4%BA%8C%E8%BF%9B%E5%88%B6&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-91354385.first_rank_v2_pc_rank_v29_10&spm=1018.2226.3001.4187)
2.  感谢<<自己动手写JAVA虚拟机>>(作者:张秀宏)